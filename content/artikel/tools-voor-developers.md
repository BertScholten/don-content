---
summary: "De centrale lijst met tools die gebruikt kunnen worden bij het bouwen van projecten voor de publieke sector."
published_at: 2023-11-21
topics: ["Tools","IDE","SDK"]
---
# Tools

Op deze pagina vind je tools die jouw dagelijkse werk makkelijker maken.

## CLI Tools 

### API Design Rules (ADR) validator

De API Design Rules (ADR) Validator is een tool die de kwaliteit van een API meet aan de hand van een aantal meetbare regels. De ADR-Validator maakt het mogelijk om tijdens het ontwikkelproces, zowel lokaal als in CI, een API te testen. 

[Gitlab ADR Validator](https://gitlab.com/commonground/don/adr-validator)

## Desktop apps

Welke desktop apps kan je gebruiken bij het developen?

### Visual Studio Code

Visual Studio Code (VSCode) is een populaire open-source code-editor ontwikkeld door Microsoft. De voordelen van deze editor zijn als volgt:

- Open-source en gratis
- Lichtgewicht
- Groot ecosysteem met veel plugins
- Cross-platform: beschikbaar voor Windows, Mac en Linux

[VSCode](https://code.visualstudio.com/)

### DBeaver

DBeaver is een krachtige en veelzijdige opensource databasebeheer- en SQL-clienttool die compatibel is met verschillende databases. De applicatie heeft de volgende voordelen:
- Multi-database-ondersteuning
- Gegevensvisualisatie
- Connection management

[DBeaver](https://dbeaver.io/)
