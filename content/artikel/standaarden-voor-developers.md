---
summary: "Wat zijn goede standaarden voor developers binnen de Nederlandse overheid?"
published_at: 2023-11-21
topics:
  - standaarden
---
# Deze overheidsstandaarden wil je als developer niet missen

## API Strategie

De API Strategie van de Nederlandse overheid bevat afspraken en standaarden om API’s te ontwerpen, te beveiligen, toegankelijk te maken en te gebruiken. 

[Standaard](https://docs.geostandaarden.nl/api/API-Strategie/)

Developers kunnen deze delen van de API strategie nu al implementeren:

### REST-API Design Rules

Deze design rules beschrijven een set regels waar een REST API aan zou moeten voldoen.

[Standaard](https://gitdocumentatie.logius.nl/publicatie/api/adr/#normative-design-rules)

Developers kunnen testen of hun APIs voldoen aan de (meetbaare) Design Rules met de [API Design Rules Validator](https://gitlab.com/commonground/don/adr-validator).

### Transport Security Module

TLS en HTTP veiligheidsregels waar een API aan zou moeten voldoen.

[Standaard](https://geonovum.github.io/KP-APIs/API-strategie-modules/transport-security/#transport-security)

Developers kunnen testen of hun APIs voldoen aan een deel van de deze veiligheidsregels met de TLS module van de [API Design Rules Validator](https://gitlab.com/commonground/don/adr-validator).

## Federated Service Connectivity (FSC)

De FSC-standaard beschrijft een manier om technisch interoperabele API gateway functionaliteit te implementeren.

[Standaard](https://commonground.gitlab.io/standards/fsc/)

Developers kunnen testen of ze de FSC standaard goed hebben geimplementeerd met de [FSC validator](https://gitlab.com/digilab.overheid.nl/ecosystem/fsc-validator).

## Data Catalog Vocabulary (DCAT)

Datasets en diensten van de nederlandse overheid moeten worden gedocumenteerd volgens DCAT versie 2.

[Standaard](https://www.w3.org/TR/vocab-dcat-2/)

Developers kunnen testen of hun dienst of dataset gedocumenteerd volgens DCAT versie 2 met de [DCAT-v2 validator](https://digilab.overheid.nl/docs/ecosysteem/dcat-v2-validator/)
